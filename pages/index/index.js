//index.js
var inputContent
var videoitems
var videoContext
var currentVideo
var start=0
Page({
  onReady: function (res) {

  },
  data: {
    videoinfos: {},
    hasMore: true,
  },
  onLoad: function () {
    console.log('onLoad')
    var that = this
    wx.showNavigationBarLoading();
    wx.showToast({
    title: '加载中',
    icon: 'loading',
    duration: 1000
   })
    requestData(that, '','minus')   
  },

  onPullDownRefresh: function () {
    this.onLoad()
  },
  onReachBottom: function () {
    var that = this
    requestData(that, '','add')
  },
  input_keyinput: function (e) {
    var that = this
    requestData(that, e.detail.value,'none')
  },
  input_change: function (e) { 
    inputContent = e.detail.value
  },
  btn_search: function () {
    console.log("inputContent--------" + inputContent)
    requestData(that, inputContent)
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  video_play: function (e) {
    videoContext = wx.createVideoContext(e.target.id + 1)
    console.log(videoContext)
  },
  video_ended: function (e) {
    videoContext = wx.createVideoContext(e.target.id)
    console.log(videoContext)
  },
})

function requestData(that, para,mothed) {
  wx.request({
    url: 'http://wsc.shuhai.cc/middle/videolist.php',
    data: {
      para: para,
      start:start,
    },
    method: 'GET', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
    header: {
      'content-type': 'application/json'
    },
    success: function (res) {
      videoitems = res.data.results
      if(mothed=='add')
      {
      start+=5
      }
     else if(mothed=='minus')
     {
       start-=5
       if(start<0)
       {
         start=0
       }
     }
     else
     {
       start=0
     }
      that.setData({
        videoinfos: videoitems,
        hasMore: true,
      })
      // success
    },
    fail: function () {
     that.setData({
        videoinfos: videoitems,
        hasMore: false,
        showLoading: true,
        start: start,
        currentVideo: 0
      })
    },
    complete: function () {
      // complete     
      wx.stopPullDownRefresh()
      wx.hideNavigationBarLoading()     
    }
  })
}
